from __future__ import unicode_literals

from django.apps import AppConfig


class CourseTagsConfig(AppConfig):
    name = 'course_tags'
